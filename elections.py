
import argparse
import datetime
from time import sleep

import yaml
import analysis
import static_site
import deploy

from live_results import process_live_election
from distributed import collect_imgs


def run_elections(elections_obj):
    for election in elections_obj:
        if election.get('live', False):
            if 'analysis' in election:
                del election['analysis']
                del election['stale']
            process_live_election(election)
        if 'analysis' not in election:
            election['analysis'] = analysis.analyze(election.get('filename'),
                                                    election_name=election['name'],
                                                    img_outdir='out',
                                                    geojson_file=election['geojson'],
                                                    image_suffix=election.get('image_suffix'),
                                                    turnout_source=election.get('turnout_source'),
                                                    turnout_suffix=election.get('turnout_suffix'),
                                                    scaffold=election.get('use_scaffold', False))
    # run deploy after images generated so that they are there before the html
    collect_imgs()
    deploy.run()
    static_site.generate_static_site(elections_obj)
    deploy.run()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--live', dest='live', default=0, type=int,
                        help='run live, pausing X seconds between runs')
    args = parser.parse_args()
    with open('elections.yaml', 'r') as f:
        elections = yaml.safe_load(f)
    if args.live == 0:
        run_elections(elections)
    else:
        while True:
            print("Live mode! Running!")
            start = datetime.datetime.utcnow()
            run_elections(elections)
            for election in elections:
                election['stale'] = True
            sleeptime = args.live - (datetime.datetime.utcnow() - start).total_seconds()
            print("Sleeping for {} seconds...".format(sleeptime))
            if sleeptime > 0:
                sleep(sleeptime)


if __name__ == '__main__':
    main()
