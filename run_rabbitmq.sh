#!/bin/bash

docker run -d --name philly_election_map_rabbitmq \
   --publish=127.0.0.1:15672:15672 \
   --publish=127.0.0.1:5672:5672 \
   --publish=127.0.0.1:5671:5671 \
   rabbitmq:3.7-management
