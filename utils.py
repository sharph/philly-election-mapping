import datetime
import pytz

utc = pytz.utc
eastern = pytz.timezone('US/Eastern')


def sanitize_name(name):
    replace_chars = '#- !,&'
    return ''.join(char if char not in replace_chars else '_' for char in name).lower()


def uncapitalize(text):
    SKIP = ('', 'and', 'the', 'of', 'in')
    if text.upper() != text:
        # text is already uncapitalized, trust that input is better than what
        # we can do
        return text

    text = text.lower()
    for symbol in (' ', '-'):
        text = text.split(symbol)
        text = [segment[0].upper() + segment[1:] if segment not in SKIP else segment
                for segment in text]
        text = symbol.join(text)

    return text


def format_time(dt):
    # convert to US eastern
    dt = utc.localize(dt)
    dt = dt.astimezone(eastern)
    return dt.strftime('%c %Z%z')


def is_it_after(after_str, cmp=None):
    if cmp is None:
        cmp = utc.localize(datetime.datetime.utcnow()).astimezone(eastern)
    else:
        cmp = utc.localize(cmp).astimezone(eastern)
    after_time = datetime.datetime.strptime(after_str, '%Y-%m-%d %H:%M')
    after_time = eastern.localize(after_time)
    return cmp > after_time
