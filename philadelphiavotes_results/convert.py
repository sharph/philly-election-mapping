import os
import csv


def convert(fn, dest):
    print('converting {} to {}'.format(fn, dest))
    assert os.system("ssconvert -S '{}' /tmp/CONVERT.csv".format(fn)) == 0
    with open(dest, 'w') as f:
        fieldnames = ['WARD', 'DIVISION', 'OFFICE', 'CANDIDATE', 'VOTES', 'PARTY']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for filename in os.listdir('/tmp/'):
            if not filename.startswith('CONVERT'):
                continue
            with open('/tmp/' + filename, 'r') as r:
                reader = csv.reader(r)
                for row in reader:
                    if row[1] == 'Ward-Division':
                        idx = row
                        print('  ' + idx[0].split('<')[0])
                    elif '-' in row[1]:
                        ward, division = row[1].split('-')
                        for i, candidate in enumerate(idx[2:], 2):
                            writer.writerow({
                                'WARD': ward,
                                'DIVISION': division,
                                'OFFICE': idx[0].split('<')[0],
                                'CANDIDATE': candidate,
                                'VOTES': row[i],
                                'PARTY': 'NULL'
                            })
            os.unlink('/tmp/' + filename)
    print('done converting')
