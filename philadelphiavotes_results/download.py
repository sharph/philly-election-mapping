from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
import time

def enable_download_headless(browser, download_dir):
    browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd':'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    browser.execute("send_command", params)

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")
chrome_options.add_argument("--disable-notifications")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--verbose')
chrome_options.add_experimental_option("prefs", {
        "download.default_directory": "/tmp/",
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing_for_trusted_sources_enabled": False,
        "safebrowsing.enabled": False
})
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-software-rasterizer')

driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="/usr/bin/chromedriver")

download_dir = "/tmp/"
enable_download_headless(driver, download_dir)


def download():
    for filename in os.listdir('/tmp/'):
        if filename.endswith('xlsx'):
            os.unlink('/tmp/' + filename)
    driver.get('https://results.philadelphiavotes.com/ResultsExport.aspx?')
    link = driver.find_element_by_css_selector('#MainContent_rptCountyExport_LinkButton1_0')
    link.click()
    for _ in range(10):
        time.sleep(2)
        for filename in os.listdir('/tmp/'):
            if filename.endswith('xlsx'):
                return '/tmp/' + filename
    raise Exception('couldnt download')


if __name__ == '__main__':
    download()
