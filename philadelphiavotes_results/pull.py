import json
import requests
import csv

ids = [
    ('JUD', '6', '6', '04', 'DEM'),
    ('JUD', '7', '7', '04', 'REP'),
    ('JUD', '8', '8', '04', 'DEM'),
    ('JUD', '9', '9', '04', 'REP'),
    ('JUD', '10', '10', '04', 'DEM'),
    ('JUD', '11', '11', '04', 'REP'),
    ('JUD', '12', '12', '04', 'DEM'),
    ('JUD', '13', '13', '04', 'REP'),
    ('JUD', '14', '14', '04', 'DEM'),
    ('JUD', '15', '15', '04', 'REP'),
    ('CTY', '16', '16', '04', 'DEM'),
    ('CTY', '17', '17', '04', 'REP'),
    ('CTY', '18', '18', '04', 'DEM'),
    ('CTY', '19', '19', '04', 'REP'),
    ('QUE', '6832', '6832', '04', '0'),
    ('QUE', '6833', '6833', '04', '0'),
    ('QUE', '6834', '6834', '04', '0'),
    ('QUE', '6835', '6835', '04', '0'),
    ('QUE', '6836', '6836', '04', '0')
]

URLS = []
for thingies in ids:
    URLS.append('https://phillyresws.azurewebsites.us/ResultsAjax.svc/GetMapData?type={}&category=PREC&raceID={}&osn={}&county={}&party={}&LanguageID=1'.format(*thingies))

#URLS = [
#    'https://phillyresws.azurewebsites.us/ResultsAjax.svc/GetMapData?type=CTY&category=PREC&raceID=17&osn=17&county=04&party=REP&LanguageID=1',
#    'https://phillyresws.azurewebsites.us/ResultsAjax.svc/GetMapData?type=JUD&category=PREC&raceID=6&osn=6&county=04&party=DEM&LanguageID=1',
#    'https://phillyresws.azurewebsites.us/ResultsAjax.svc/GetMapData?type=QUE&category=PREC&raceID=6834&osn=6834&county=04&party=0&LanguageID=1'
#]

def download():
    result = []
    for url in URLS:
        print('downloading', url)
        result += requests.get(url, timeout=120).json()
    return result


def pull(dest):
    records = download()
    with open(dest, 'w') as f:
        fieldnames = ['WARD', 'DIVISION', 'OFFICE', 'CANDIDATE', 'VOTES', 'PARTY', 'REPORTING']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for record in records:
            writer.writerow({
                'WARD': record['PrecinctNumber'][:2],
                'DIVISION': record['PrecinctNumber'][2:],
                'OFFICE': record['RaceName'].split('<')[0],
                'CANDIDATE': record['calcCandidate'].rstrip(),
                'VOTES': record['calcCandidateVotes'],
                'PARTY': 'null',
                'REPORTING': 1 if record['IsReported'] else 0
            })
    print('done converting')
