
import datetime
import os.path
import os
import hashlib

from .pull import pull

last = None
lasthash = None


def hash_file(filename):
    hash = hashlib.sha256()
    with open(filename, 'rb') as f:
        hash.update(f.read())
    return hash.hexdigest()[:8]


def data_download(dest):
    global last, lasthash
    if os.path.exists(dest):
        os.unlink(dest)

    try:
        pull(dest)
    except Exception as e:
        print(e)
        return last

    thistime = datetime.datetime.now()
    thishash = hash_file(dest)
    if lasthash != thishash:
        last = thistime
        lasthash = thishash
    return last
