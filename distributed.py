
import os
import os.path
from election_map import make_map as make_map_local

from celery import Celery

broker = os.getenv('BROKER')

app = Celery('distributed', backend='rpc://', broker=broker)
app.conf.task_serializer = 'pickle'
app.conf.result_serializer = 'pickle'
app.conf.accept_content = ['pickle']
app.conf.result_accept_content = ['pickle']
app.conf.broker_connection_max_retries = 0
app.conf.broker_heartbeat = 0


waiting_on = []


@app.task
def make_map_task(*args, **kwargs):
    make_map_local(*args, **kwargs)
    with open(args[4], 'rb') as f:
        data = f.read()
    return (args[4], data)


def make_map(*args, **kwargs):
    if broker is None:
        make_map_local(*args, **kwargs)
        return
    if not os.path.exists(args[4]):
        waiting_on.append(make_map_task.delay(*args, **kwargs))


def collect_imgs():
    global waiting_on
    print('Collecting imgs...')
    for task in waiting_on:
        filename, data = task.get()
        print(filename)
        with open(filename, 'wb') as f:
            f.write(data)
    waiting_on = []
