import requests
import hashlib
import datetime

from philadelphiavotes_results import data_download

from utils import format_time, is_it_after


EXPORT_FILE = ''
TURNOUT_URL = 'https://jtannen.github.io/precinct_turnout_philadelphia.csv'

TMP_FILE = '/tmp/live_election.csv'
TURNOUT_TMP = '/tmp/turnout.csv'
DATA_SOURCE = 'https://results.philadelphiavotes.com/'


def url_download(url, dest):
    print('Downloading {}...'.format(url))
    with requests.get(url, stream=True) as f:
        with open(dest, 'wb') as out:
            for chunk in f.iter_content(chunk_size=8192):
                out.write(chunk)
    print('Done.')
    assert f.headers['Last-Modified'][-4:] in [' GMT', ' UTC']
    return datetime.datetime.strptime(f.headers['Last-Modified'][:-4], "%a, %d %b %Y %H:%M:%S")


def hash_file(filename):
    hash = hashlib.sha256()
    with open(filename, 'rb') as f:
        hash.update(f.read())
    return hash


def process_live_election(election):
    modified = data_download(TMP_FILE)
    if ('after' not in election or is_it_after(election['after'], modified) or
            election.get('scaffold', False)):
        election['data_modified'] = format_time(modified)
        election['data_checked'] = format_time(datetime.datetime.utcnow())
        election['data_source'] = DATA_SOURCE
        election['image_suffix'] = '_live_{}'.format(hash_file(TMP_FILE).hexdigest()[:8])
        election['filename'] = TMP_FILE
        election['use_scaffold'] = election.get('scaffold', False) and \
            'after' in election and is_it_after(election['after'], modified)

    if 'turnout_after' in election and is_it_after(election['turnout_after']):
        modified = url_download(TURNOUT_URL, TURNOUT_TMP)
        if is_it_after(election['turnout_after'], modified):
            election['turnout_modified'] = format_time(modified)
            election['turnout_checked'] = format_time(datetime.datetime.utcnow())
            election['turnout_source'] = TURNOUT_TMP
            election['turnout_suffix'] = '_live_{}'.format(hash_file(TURNOUT_TMP).hexdigest()[:8])
            election['turnout_url'] = TURNOUT_URL
    return election


if __name__ == '__main__':
    print(process_live_election({}))
