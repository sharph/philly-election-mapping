FROM debian:testing

RUN mkdir /app && \
    apt-get update && \
    apt-get install -y python3-pip python3-matplotlib python3-cartopy \
                       python3-scipy \
                       fonts-tlwg-loma-otf fonts-tlwg-loma-ttf \
                       optipng curl \
                       python3-selenium gnumeric

WORKDIR /app

COPY . /app

RUN pip3 install -r requirements.txt
RUN ./download_prereq_data.sh

CMD python3 elections.py
