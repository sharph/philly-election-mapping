
import json

from jinja2 import Environment, FileSystemLoader, select_autoescape
from datetime import datetime
from utils import format_time


def json_out(race, election):
    divs = {}
    for data in race['raw_data']:
        if data['Carts'][0] != data['Carts'][1]:
            continue
        if data['Choice'] not in divs:
            divs[data['Choice']] = {}
        divs[data['Choice']][data['Division']] = data['Votes']
    with open('out/{}.json'.format(race['slug']), 'wb') as f:
        f.write(json.dumps({
            'name': race['name'],
            'checked': election.get('data_checked', None),
            'modified': election.get('data_modified', None),
            'reporting_divs': race['reporting_divs'],
            'total_divs': race['total_divs'],
            'results': race['results'],
            'divs': divs
        }, indent=2).encode('utf-8'))


def generate_static_site(election_data):
    env = Environment(
        loader=FileSystemLoader('templates'),
        autoescape=select_autoescape(['html', 'xml'])
    )
    env.get_template('index.html').stream(elections=election_data).dump('out/index.html')
    for election in election_data:
        if 'stale' in election:
            continue
        env.get_template('election.html').stream(elections=election_data,
                                                 election=election,
                                                 now=format_time(datetime.utcnow())) \
            .dump('out/{}.html'.format(election['analysis']['slug']))
        if 'races' in election['analysis']:
            for race in election['analysis']['races']:
                env.get_template('race.html').stream(elections=election_data,
                                                     election=election,
                                                     race=race,
                                                     now=format_time(datetime.utcnow())) \
                    .dump('out/{}.html'.format(race['slug']))
                json_out(race, election)
