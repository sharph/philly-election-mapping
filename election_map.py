import matplotlib
matplotlib.use("Agg")

import subprocess
import os.path
from matplotlib import rcParams
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import shapely.geometry as sgeom
import collections
import random
import numpy as np
import geojson

COLORS = [
    (0, 0.5, 1),
    (0.8, 0, 0),
    (0, 0.8, 0),
    (1, 0.5, 0),
    (0.9, 0.9, 0),
    (0, 1, 0),
    (1, 0, 1),
    (0, 1, 1),
    (1, 1, 0.5),
    (0.1, 0.1, 0.1),
    (0.3, 0.3, 0.3),
    (0.5, 0.5, 0.5),
]


MAX_DENSITY = 10000
STREET_CENTERLINES = 'Street_Centerline.geojson'

rcParams['font.sans-serif'] = 'Loma'


def get_color(i, colors=COLORS):
    if i < len(colors):
        return colors[i]
    return colors[-1]


def get_random_point(poly):
    x1, y1, x2, y2 = poly.bounds
    found = False
    while not found:
        randompoint = sgeom.Point(random.random() * (x2 - x1) + x1,
                                  random.random() * (y2 - y1) + y1)
        if randompoint.within(poly):
            found = True
    return randompoint


def optimize_image(filename):
    subprocess.call(['optipng', filename])


def make_map(election_name, race_name, race_data, map_data,
             output_name, force=False, noun='votes', dot_coeff=1):
    # load street centerlines
    with open(STREET_CENTERLINES, 'rb') as f:
        streets = geojson.load(f)
    streets = [sgeom.shape(feature['geometry'])
               for feature in streets['features'] if feature['properties']['CLASS'] == 2]

    if os.path.exists(output_name) and not force:
        return
    print('generating {}, {}...'.format(election_name, race_name))
    # first, do some stats
    totals = collections.defaultdict(lambda: 0)
    indexed = collections.defaultdict(lambda: None)
    pointcollections = collections.defaultdict(list)
    totalvotecounts = collections.defaultdict(lambda: 0)
    carts = {}
    for record in race_data:
        totals[record['Choice']] += record['Votes']
        totalvotecounts[record['Division']] += record['Votes']
        indexed[(record['Choice'], record['Division'])] = record
        carts[record['Division']] = record['Carts']
    dot_value = int(sum(totalvotecounts.values()) / (MAX_DENSITY * dot_coeff + 1))
    if dot_value < 1:
        dot_value = 1

    fig = plt.figure(figsize=[9, 10])
    ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.epsg(2272))

    ax.background_patch.set_visible(False)
    ax.outline_patch.set_visible(False)

    included_divisions = []
    incomplete_included_divisions = []
    excluded_divisions = []
    for division in map_data:
        division['geom'] = sgeom.shape(division['geometry'])
        division_key = division['properties']['Division']
        include_division = False
        for vote_choice in totals.keys():
            if indexed[vote_choice, division_key] is not None:
                num_dots = int((float(indexed[vote_choice, division_key]['Votes']) +
                                (random.random() * dot_value))  # mitigate against threshold effect
                               / dot_value)
                for n in range(num_dots):
                    pointcollections[vote_choice].append(get_random_point(division['geom']))
                include_division = True
        if include_division:
            if carts[division_key][0] >= carts[division_key][1]:
                included_divisions.append(division)
            else:
                incomplete_included_divisions.append(division)
        else:
            excluded_divisions.append(division)

    mp = sgeom.MultiPolygon([division['geom'] for division in included_divisions +
                             incomplete_included_divisions])
    bounds = mp.bounds
    ax.set_extent([bounds[n] for n in [0, 2, 1, 3]], ccrs.Geodetic())
    ax.add_geometries(map(lambda x: x['geom'], excluded_divisions),
                      ccrs.PlateCarree(),
                      facecolor='#cccccc',
                      edgecolor='#aaaaaa',
                      linewidth=0.5, zorder=1)
    ax.add_geometries(map(lambda x: x['geom'], incomplete_included_divisions),
                      ccrs.PlateCarree(),
                      facecolor='#ffdddd',
                      edgecolor='#bbbbbb',
                      linewidth=0.5, zorder=1)
    ax.add_geometries(map(lambda x: x['geom'], included_divisions),
                      ccrs.PlateCarree(),
                      facecolor='#eeeeee',
                      edgecolor='#bbbbbb',
                      linewidth=0.5, zorder=1)
    ax.add_geometries(streets,
                      ccrs.PlateCarree(),
                      edgecolor='#4a4a4a',
                      facecolor=(0,0,0,0),
                      linewidth=1.5, zorder=4)

    choices = totals.keys()
    choices = sorted(choices, key=lambda x: -totals[x])
    colors = COLORS[:len(choices)]

    points = []
    labels = []
    patches = []
    for color, choice in enumerate(choices):
        patches.append(mpatches.Circle((0, 0), facecolor=get_color(color, colors=colors)))
        labels.append(u'{} ({})'.format(choice, totals[choice]))
        for point in pointcollections[choice]:
            points.append((point.x, point.y, get_color(color, colors=colors)))

    points.sort(key=lambda x: random.random())
    # randomize points to avoid one vote choice plastering another
    ax.scatter(np.array([p[0] for p in points]),
               np.array([p[1] for p in points]), marker='.',
               transform=ccrs.PlateCarree(),
               s=15, c=np.array([p[2] for p in points]),
               edgecolors='none',
               zorder=2)

    patches.append(mpatches.Circle((0, 0), facecolor=(0, 0, 0, 0)))
    labels.append('1 dot = {} {}'.format(dot_value, noun))
    ax.legend(patches, labels, fancybox=False,
              loc='lower right')
    plt.savefig(output_name,
                bbox_inches='tight', transparent=True)
    optimize_image(output_name)
    ax.set_title('{}\n{}'.format(election_name, race_name),
                 pad=20, zorder=10, loc='left', fontsize='xx-large', fontweight='bold')
    plt.close(fig)
