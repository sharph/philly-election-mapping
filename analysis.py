import os.path
import collections
import unicodecsv as csv
import geojson

from distributed import make_map
from utils import sanitize_name, uncapitalize


def standardize_geojson_division(geojson_obj):
    divisions = geojson_obj['features']
    for division in divisions:
        for key in ('WARD_DIVSN', 'DIVISION_NUM'):
            if key in division['properties']:
                division['properties']['Division'] = (division['properties'][key][:2] + '-' +
                                                      division['properties'][key][2:])
                break
    return geojson_obj


def standardize_record_asv(record):
    carts = [1, 1]
    for key in ('CartsProcessed_CartsExpected', 'Party_Name'):
        try:
            carts = [int(x) for x in record[key].split('_')]
            break
        except ValueError:
            pass
        except AttributeError:
            pass
        except KeyError:
            pass
    try:
        return {
            'FirstName': None if record['First_Name'] == 'NULL' else record['First_Name'],
            'LastName': None if record['Last_Name'] == 'NULL' else record['Last_Name'],
            'Votes': int(record['Vote_Count']),
            'Carts': carts,
            'Choice': record['Tape_Text'],
            'Race': record['Office_Prop Name'],
            'Division': record['Precinct_Name']
        }
    except TypeError:
        return None


def get_first(record, *keys):
    for key in keys:
        if key in record:
            return record[key]


def load_election_data_new_format(filename, scaffold=False):
    races = collections.defaultdict(lambda: {'raw_data': []})
    with open(filename, 'rb') as f:
        sheet = list(csv.reader(f))
        header, body = sheet[:2], sheet[2:]
    data = []
    for row in body:
        if row[2] == 'COUNTY TOTALS':
            continue
        carts = [0 if sum(int(x) for x in row[3:] if x != '') == 0 else 1, 1]
        for i, col in enumerate(row):
            if i < 3 or col == '':
                continue
            data.append({
                'Votes': 0 if scaffold else int(col),
                'Carts': [0, 1] if scaffold else carts,
                'Choice': header[1][i].strip(),
                'Race': header[0][i].strip(),
                'Division': row[2].strip()
            })
            if header[0][i].strip() == 'COUNCIL AT-LARGE':
                data.append({
                    'Votes': 0 if scaffold else int(col),
                    'Carts': [0, 1] if scaffold else carts,
                    'Choice': header[1][i].strip(),
                    'Race': 'Council At-Large ' + ('(Democratic)' if '(DEMOCRATIC)' in header[1][i]
                                                   else '(Non-democratic)'),
                    'Division': row[2].strip()
                })
    for record in data:
        races[record['Race']]['raw_data'].append(record)
    return races


def load_election_data(filename, scaffold=False):
    races = collections.defaultdict(lambda: {'raw_data': []})
    if filename.endswith('txt'):
        with open(filename, 'rb') as f:
            for row in csv.DictReader(f, delimiter='@'):
                row = standardize_record_asv(row)
                if row is not None and row['Race'] is not None:
                    if scaffold:
                        races[row['Race']]['raw_data'].append(make_scaffold(row))
                    else:
                        races[row['Race']]['raw_data'].append(row)
        return races
    votecounts = collections.defaultdict(lambda: 0)
    with open(filename, 'rb') as f:
        try:
            f.read().decode('utf-8-sig')
            encoding = 'utf-8-sig'
        except UnicodeDecodeError:
            encoding = 'iso-8859-1'
            print('Guessing {} is {}'.format(filename, encoding))
    with open(filename, 'rb') as f:
        for row in csv.DictReader(f, encoding=encoding):
            carts = 1
            if 'REPORTING' in row:
                carts = int(row['REPORTING'])
            votecounts[(
                row['WARD'],
                get_first(row, 'DIVISION', 'DIV'),
                get_first(row, 'OFFICE', 'CATEGORY'),
                get_first(row, 'CANDIDATE', 'SELECTION', 'CHOICE'),
                row['PARTY'],
                carts
            )] += int(get_first(row, 'VOTES', 'VOTE COUNT'))
    data = [{
        'Votes': votecount,
        'Carts': [record[5], 1],
        'Choice': record[3],
        'Race': record[2],
        'Division': str(record[0]).zfill(2) + '-' + str(record[1]).zfill(2)
    } for record, votecount in votecounts.items()]
    for record in data:
        if record['Race'].lower() != 'no vote':
            if scaffold:
                races[record['Race']]['raw_data'].append(make_scaffold(record))
            else:
                races[record['Race']]['raw_data'].append(record)
    return races


def load_turnout_data(filename):
    turnout = []
    with open(filename, 'rb') as f:
        for row in csv.DictReader(f):
            turnout.append({
                'Votes': float(row['turnout']),
                'Carts': [1, 1],
                'Choice': 'Turnout',
                'Race': 'Turnout',
                'Division': row['precinct'][:2] + '-' + row['precinct'][2:]
            })
    return turnout


def make_scaffold(record):
    record['Votes'] = 0
    record['Carts'] = [0, 1]
    return record


def analyze(in_file, geojson_file='Political_Divisions.geojson',
            election_name='', img_outdir=None, image_ext='png',
            image_suffix=None, turnout_source=None, turnout_suffix='',
            scaffold=False):
    election = {}
    election['name'] = election_name
    election['slug'] = sanitize_name(election_name)

    # turnout
    if turnout_source:
        filename = sanitize_name(election_name) + '_turnout' + \
            turnout_suffix + '.' + image_ext
        election['turnout'] = {
            'raw_data': load_turnout_data(turnout_source),
            'map': filename
        }
        with open(geojson_file) as f:
            geojson_obj = geojson.load(f)
        geojson_obj = standardize_geojson_division(geojson_obj)
        make_map(election_name, 'Estimated live turnout',
                 election['turnout']['raw_data'],
                 geojson_obj['features'],
                 os.path.join(img_outdir, filename),
                 noun='voters')
    if not in_file:
        return election

    # populate election['races'] with raw data
    try:
        races = load_election_data(in_file, scaffold=scaffold)
    except KeyError:
        races = load_election_data_new_format(in_file, scaffold=scaffold)

    for race_name, race in races.items():
        race['name'] = uncapitalize(race_name)

    election['races'] = races.values()

    for race in election['races']:
        # sanitized name
        race['slug'] = sanitize_name(election_name + '_' + race['name'])

        # calculate division reporting
        divs_reporting = {}
        for record in race['raw_data']:
            divs_reporting[record['Division']] = record['Carts'][0] >= record['Carts'][1]

        race['total_divs'] = len(divs_reporting.values())
        race['reporting_divs'] = len([x for x in divs_reporting.values() if x is True])

        # maps
        suffix = image_suffix if image_suffix else ''
        filename = sanitize_name(election_name + '_' + race['name']) + \
            suffix + '.' + image_ext
        with open(geojson_file) as f:
            geojson_obj = geojson.load(f)
        geojson_obj = standardize_geojson_division(geojson_obj)
        make_map(election_name, race['name'], race['raw_data'],
                 geojson_obj['features'],
                 os.path.join(img_outdir, filename),
                 dot_coeff=float(race['reporting_divs']) / race['total_divs'])
        race['map'] = filename

        # vote count
        race['results'] = collections.defaultdict(lambda: 0)
        for record in race['raw_data']:
            race['results'][record['Choice']] += int(record['Votes'])

        race['sorted_choices'] = sorted(race['results'].keys(),
                                        key=lambda x: -race['results'][x])

        # total
        race['total_votes'] = sum(race['results'].values())

    election['races'] = sorted(election['races'], key=lambda race: -race['total_votes'])

    return election
