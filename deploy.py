import s3_deploy.deploy
import os


def run():
    bucket = os.getenv('S3_BUCKET')
    if not bucket:
        print('S3 bucket not set. Skipping deploy!')
        return
    config = {'s3_bucket': bucket,
              'site': 'out',
              'cache_rules': [{'match': '*', 'maxage': '2 minutes'}]}
    s3_deploy.deploy.deploy(config, '', False, False)
